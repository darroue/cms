# !/bin/bash
rm -rf .[!.]* ..?* \
  *.{js,json,yml,sh} \
  Docker* \
  node_modules \
  tmp \
  vendor/cache
