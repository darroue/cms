# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def path_helper(path)
  Rails.application.routes.url_helpers.send(path)
end

def format_message(description, item_type = nil)
  message = if item_type.present?
              "#{item_type.to_s.capitalize}: #{description}"
            else
              " - #{description}"
            end

  puts message
end

puts '------- Seeding  -------'

# Menu
[
  {
    name: 'main',
    description: 'Main menu',
    menu_items_attributes: [
      {
        title: 'New Post',
        description: 'Create a new post',
        link: path_helper(:new_post_path)
      },
      {
        title: 'New Page',
        description: 'Create a new page',
        link: path_helper(:new_page_path)
      },
      {
        title: 'Posts',
        description: 'List of all existing posts',
        link: path_helper(:posts_path)
      },
      {
        title: 'Pages',
        description: 'List of all existing pages',
        link: path_helper(:pages_path)
      },
      {
        title: 'Settings',
        description: 'Allows you to manage application settings',
        link: path_helper(:admin_path)
      }
    ]
  }
].each do |menu_attrs|
  menu = Menu.find_or_initialize_by menu_attrs.slice(:name)

  next if menu.persisted?

  format_message(menu.description, :menu)
  format_message(:creating)

  menu.update(menu_attrs)
end

[
  {
    title: 'Main Widget',
    description: 'You can place anything here',
    body: 'Some text maybe?'
  }
].each do |widget_attrs|
  widget = Widget.find_or_initialize_by widget_attrs.slice(:title)

  next if widget.persisted?

  format_message(widget.description, :widget)
  format_message(:creating)

  widget.assign_attributes(widget_attrs)
  widget.save!(validate: false)
end

# Roles
%i(administrators everyone users).each do |role_name|
  formated_role_name = role_name.capitalize

  role = Role.find_or_initialize_by(name: formated_role_name)
  format_message(formated_role_name, :role)

  unless role.persisted?
    format_message(:creating)
    role.assign_attributes(name: formated_role_name, protected: true, active: true)
  end

  format_message(:updating_permissions)
  role.assign_attributes(permissions: Role.default_permissions(role_name))

  role.save!(validate: false)
end

# Administrator
admin = User.find_or_initialize_by nickname: 'Administrator'
unless admin.persisted?
  format_message(admin.nickname, :user)
  format_message(:creating)

  admin.assign_attributes(
    active: true, first_name: 'Mr.', last_name: 'Admin', email: 'admin',
    password: ENV.fetch('ADMIN_PASSWORD') { 'admin_pass' },
    role_ids: Role.where(name: %w(Administrators Users)).pluck(:id)
  )
  admin.save!(validate: false)
end

# Settings
{
  site_name: 'CMS',
  theme: 'default'
}.each_pair do |name, value|
  setting = Setting.find_or_initialize_by(name: name)

  format_message(name, :setting)

  unless setting.persisted?
    format_message(:creating)
    setting.assign_attributes(name: name, value: value, default_value: value)
  end

  setting.save!
end

puts '----- Seeding Done -----'
