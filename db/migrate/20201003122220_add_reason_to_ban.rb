class AddReasonToBan < ActiveRecord::Migration[6.0]
  def change
    add_column :bans, :reason, :string
  end
end
