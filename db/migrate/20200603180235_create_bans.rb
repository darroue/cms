class CreateBans < ActiveRecord::Migration[6.0]
  def change
    create_table :bans do |t|
      t.boolean :active
      t.datetime :valid_until
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
