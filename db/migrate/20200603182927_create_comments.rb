class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.boolean :active
      t.text :body
      t.integer :commentable_id
      t.string :commentable_type
      t.references :replying_to
      t.references :author
      t.references :user
      t.references :parent

      t.timestamps
    end
  end
end
