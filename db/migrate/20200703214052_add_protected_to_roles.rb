class AddProtectedToRoles < ActiveRecord::Migration[6.0]
  def change
    add_column :roles, :protected, :boolean, default: false
  end
end
