class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.boolean :active
      t.string :first_name
      t.string :last_name
      t.string :nickname
      t.string :sex

      t.timestamps
    end
  end
end
