class CreateJoinTableContentsComments < ActiveRecord::Migration[6.0]
  def change
    create_join_table :contents, :comments do |t|
      t.index [:content_id, :comment_id]
      t.index [:comment_id, :content_id]
    end
  end
end
