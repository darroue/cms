class CreateRatings < ActiveRecord::Migration[6.0]
  def change
    create_table :ratings do |t|
      t.decimal :score
      t.integer :ratable_id
      t.string :ratable_type

      t.timestamps
    end
  end
end
