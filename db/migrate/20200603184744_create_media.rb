class CreateMedia < ActiveRecord::Migration[6.0]
  def change
    create_table :media do |t|
      t.boolean :active
      t.string :name
      t.text :description
      t.string :link
      t.string :media_type
      t.references :parent

      t.timestamps
    end
  end
end
