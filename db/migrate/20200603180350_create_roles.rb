class CreateRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :roles do |t|
      t.boolean :active
      t.string :name
      t.text :permissions

      t.timestamps
    end
  end
end
