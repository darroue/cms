class CreateContents < ActiveRecord::Migration[6.0]
  def change
    create_table :contents do |t|
      t.boolean :active
      t.string :title
      t.text :description
      t.text :body
      t.datetime :published_at
      t.string :content_type
      t.references :author
      t.references :user
      t.references :parent

      t.timestamps
    end
  end
end
