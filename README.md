# About

What is CMS? CMS - shortcut for content management system is web application I made.

Project is written in Ruby on Rails, ``open source`` (free to use any way you like to).

*I use it myself as system for my own blog.*

# Features

I will be adding new features based on what I currently need for my web. If you feel like something is missing, then you can create [pull request](https://bitbucket.org/darroue/cms/pull-requests/new) and if I like it, then I might add it into project.

## Currently working features:

### Users

* Registration with email confirm option and reCaptcha.
* Users can change their account details, set Avatar, delete their own account.
* You can also give user temporary or permanent Ban blocking him from login and hiding all his comments.

### Roles

* Each role has its own permissions.
* What user can do is based on his membership in those roles

### Settings

* In settings you can manage main menu, users and roles

### Pages, Posts, Tags, Comments and Widget

* These are versioned.
    * Meaning that you can go back to previous version if you want to.
* Pages and Posts can have Tags.
    * You can then access specific tags showing contents with same tag.
* Reply to comment will nest new comment under the one you're replying to.
* There is currently only one Widget in side panel.

### Editor

* Project uses [Quill](https://quilljs.com/) as content editor

### Images, Image Galleries

* Images are stored on disk, even if you place them in content body.
    * You can upload multiple images at once.
    * Images are replaced by placeholders, allowing you to specify when to display them more easily.
        * Placeholders are converted to images when content index or detail is displayed.
        * You can specify target image size and rotation, server will then send image in this variant.
        * Images without width being specified are loaded based on available block width where image is placed. That means if you upload image with width of 1000px and block where it is placed is only 300px wide, then JS will ask server to send image with maximum width of 300px. It's there to reduce traffic.

* Image Galleries takes IDs of images and then render thumbnails of images.
    * Full image is loaded with [lightgallery](https://www.lightgalleryjs.com/) when you click at thumbnail.

# Usage

## Production

1) Create new directory on your server and place [docker-compose.yml](docker-compose.yml) and ``.env`` (created from [.env.example](.env.example)) into this directory

2) Update [.env](.env) - **set all required variables**.

3) Project uses [Traefik](https://doc.traefik.io/traefik/) for serving data trough HTTPS(SSL)

  * ~~You can use my other repository for setting it up.~~ (TODO)
  * If you want to use other proxy, its up to you

4) Create and seed DB
    ``` bash
    docker-compose run --rm web bin/rails db:setup
    ```

5) That is all. You can now run server with:
    ``` bash
    docker-compose up
    ```

And access it on address specified in variable ``HOSTNAME`` in [.env](.env) file

Use ``admin`` as username and ``ADMIN_PASSWORD`` as password

## Development

1) Clone repository into new folder and place ``.env`` (created from [.env.example](.env.example)) into this directory

2) Update [.env](.env) - **set all required variables**.

3) Pull images with (so you dont have to build them):
    ``` bash
    docker-compose -f docker-compose-dev.yml pull
    ```

4) Create and seed DB
    ``` bash
    docker-compose -f docker-compose-dev.yml run --rm web bundle config path vendor/bundle && bundle cache && bundle exec bin/rails db:setup
    ```

5) That is all. You can now run server with:
    ``` bash
    docker-compose -f docker-compose-dev.yml up
    ```

And access it at http://localhost:3000

Use ``admin`` as username and ``ADMIN_PASSWORD`` as password

---

# Contact

If you have any questions or just want to say something nice to me, you can do that at darroue@gmail.com

Created by Petr Radouš in 2021
