Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: 'posts#index'

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions'
  }

  # Error routes
  %w(404 422 500 503).each do |code|
    get code, controller: :errors, action: :show, code: code
  end

  %i(pages posts).each do |resource_name|
    get :"unpublished_#{resource_name}", controller: resource_name, action: :unpublished

    resources resource_name do
      resources :comments do
        member do
          post :get_versions
          post :restore
        end
      end

      resources :tags, only: %i(destroy) do
        collection do
          post :create_tags, controller: :tags
          delete :delete_tags, controller: :tags
        end
      end
    end
  end

  %i(pages posts widgets).each do |resource_name|
    resources resource_name do
      member do
        post :restore
      end

      resources :images, only: %i(create destroy)
    end
  end

  resources :tags, only: %i(show index)
  resources :images, only: %i(show)
  get :search, controller: :search, action: :show

  scope :admin do
    get '/', to: 'settings#index', as: :admin
    scope :settings, only: %i(update index) do
      get '/', to: 'settings#index'
      get :generic, controller: :settings, as: :generic_settings
      post :generic, action: :update, controller: :settings, as: :update_generic_settings
    end
    resources :roles
    resources :users
    resources :menus
    resources :widgets do
      member do
        post :restore
      end
    end
  end
end
