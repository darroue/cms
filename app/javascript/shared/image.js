import imageLoader from '../images/loader.svg'

var windowWidth;
var loadedImages = {};

$(document).on('turbolinks:load', function () {
  window.loadImagesWithJs(true);
});

window.preloadImage = function ($image, url) {
  const img = new Image();
  const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  };

  $(img).on('load', function () {
    $image.attr('src', img.src);
  });

  img.src = url;
};

window.loadImage = function ($image, force) {
  const attributes = {};

  $($image.get(0).attributes).each(function (_, attribute) {
    if (!['class'].includes(attribute.name)) {
      attributes[attribute.name] = attribute.value;
    }
  });

  if (!attributes.width) {
    attributes.width = Math.ceil(($image.parent().width()) / 25) * 25;
  }

  const imageId = attributes.id;

  if (!force && loadedImages[imageId] && loadedImages[imageId].width && (loadedImages[imageId].width >= attributes.width)) {
    // We dont want to load image if bigger or same image was already loaded
    return;
  } else {
    // Image weren't yet loaded or was smaller
    loadedImages[imageId] = { width: attributes.width};
  }

  delete attributes.id;
  const src = `/images/${imageId}?` + $(Object.keys(attributes)).map(function (_, key) { return `${key}=${attributes[key]}`;}).toArray().join('&');

  // Show loader only once
  if (Math.max.apply(Math, [$image.width(), $image.height()]) == 0) {
    $image.attr('src', imageLoader);
  }

  window.preloadImage($image, src);

  if (!$image.hasClass('img-thumbnail') && !$image.hasClass('non-responsive')) {
    $image.addClass('responsive');
  }
};

window.loadImagesWithJs = function (force = false) {
  $('.load-with-js').each(function (_, image) {
    window.loadImage($(image), force);
  });
};

$(window).resize(function () {
  clearTimeout(window.resizedFinished);
  window.resizedFinished = setTimeout(function () {
    if ($(window).width() != windowWidth) {
      window.loadImagesWithJs();
      windowWidth = $(window).width();
    }
  }, 500);
});

window.addImageLoader = function ($image) {
  if ($image.attr('width') == undefined) {
    $image.attr('style', 'min-width: -webkit-fill-available; max-width: -webkit-fill-available;');
  }

  $image.attr('src', imageLoader);
};
