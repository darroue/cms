import lightGallery from 'lightgallery';
import lgZoom from 'lightgallery/plugins/zoom';
import lgFullscreen from 'lightgallery/plugins/fullscreen';
import lgRotate from 'lightgallery/plugins/rotate';

$(document).on('turbolinks:load', function () {
  $('.lightgallery').each(function () {
    lightGallery(this, {
      plugins: [lgZoom, lgFullscreen, lgRotate],
      speed: 500
    });
  });
});
