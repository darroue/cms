$(document).on('turbolinks:load', function () {
  $(".submit_on_return").on("keypress", function(event){
    if (event.key == 'Enter') {
      event.currentTarget.form.submit()
    }
  })
})

window.expandCard = function(element) {
  row = $(element).closest(".row")
  areaToExpant = row.children().first()
  areaToHine = row.children().last()
  expand = true

  if (areaToExpant.attr("previousClass")){
    areaToExpant.attr("class", areaToExpant.attr("previousClass"))
    areaToExpant.removeAttr("previousClass")
    areaToHine.show()
    expand = false
  } else {
    areaToHine.hide()
    previousClass = areaToExpant.attr("class")
    areaToExpant.attr("previousClass", previousClass)
    areaToExpant.attr("class", "col-md-12")
  }

  $(element).toggleClass("fa-compress-alt", expand)
  $(element).toggleClass("fa-expand-alt", !expand)
}
