window.comment_toggle = function (element, enable, commentSubBlock) {
  var clazz = "hidden";
  var commentBlock = ".comment-block";

  $(".comment-reply, .comment-edit, .comment-restore").toggleClass(clazz, true);
  $(element).closest(commentBlock).find(commentSubBlock).toggleClass(clazz, !enable);
}
window.comment_toggle_reply_view = function (element, enable) {
  window.comment_toggle(element, enable, ".comment-reply")
}

window.comment_toggle_edit_view = function (element, enable) {
  window.comment_toggle(element, enable, ".comment-edit")
}

window.comment_toggle_restore_view = function (element, enable) {
  var commentBlock = ".comment-block";
  var commentSubBlock = ".comment-restore"

  window.comment_toggle(element, enable, commentSubBlock)
  
  comment_id = $(element).closest("[comment_id]").attr('comment_id')
  $version_element = $(element).closest(commentBlock).find(".version-list")

  $.ajax({
    url: window.location.pathname + '/comments/%id/get_versions'.replace('%id', comment_id),
    method: 'post',
    success: function(result){
      container = $('<div/>')
      input = $('<input class="version_to_restore" type="hidden">')
      container.append(input)
      $(JSON.parse(result)).each(
        function(i, e){
          p = $('<p onclick="window.comment_restore(this)" class="comment_version"/>')
          div = $('<div>')

          p.attr("comment_id", comment_id)
          p.attr("version_id", e.id)
          p.append(e.updated_at)
          div.append(e.body)
          p.append(div)
          container.append(p)
        }
      )
      $version_element.html(container)
    }
  })
}

window.comment_restore = function (element) {
  comment_id = $(element).attr('comment_id')
  version_id = $(element).attr('version_id')

  $.ajax({
    url: window.location.pathname + '/comments/%id/restore'.replace('%id', comment_id),
    data: {
      version_id: version_id
    },
    method: 'post'
  })
}