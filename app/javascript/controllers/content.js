require('../shared/image');
require('../shared/gallery');

import Quill from 'quill';

$(document).on('turbolinks:load', function () {
  var editorElementSelector = '#editor';
  var editorElement = $(editorElementSelector);
  var containerElement = $('.ql-editor');

  if (editorElement.length && !containerElement.length) {
    if (!editorElement.attr('readonly')) {
      var editorColors = [
        '#000000',
        '#e60000',
        '#ff9900',
        '#ffff00',
        '#008a00',
        '#0066cc',
        '#9933ff',
        '#ffffff',
        '#facccc',
        '#ffebcc',
        '#ffffcc',
        '#cce8cc',
        '#cce0f5',
        '#ebd6ff',
        '#bbbbbb',
        '#f06666',
        '#ffc266',
        '#ffff66',
        '#66b966',
        '#66a3e0',
        '#c285ff',
        '#888888',
        '#a10000',
        '#b26b00',
        '#b2b200',
        '#006100',
        '#0047b2',
        '#6b24b2',
        '#444444',
        '#5c0000',
        '#663d00',
        '#666600',
        '#003700',
        '#002966',
        '#3d1466'
      ];
      var editor = new Quill(editorElementSelector, {
        theme: 'snow',
        modules: {
          toolbar: [
            [{ header: [1, 2, false] }],
            ['bold', 'italic', 'underline'],
            [{color: editorColors}, {background: editorColors}],
            ['link', 'image', 'video'],
            [{'list': 'bullet'}, {'list': 'ordered'}, 'clean']
          ]
        }
      });
    }
  }
});

window.submitOnEnter = function (event, element) {
  if (event.code == 'Enter' || event.code == 'NumpadEnter') {
    event.preventDefault();
    window.submitForm($(element).closest('form'));
  }
};

window.submitForm = function ($form) {
  $.ajax({
    type: $form.attr('method'),
    url: $form.attr('action'),
    data: $form.serialize()
  });
};

window.remove_tag = function (element, id) {
  var $li = $(element).closest('li');
  var name = $li.find('.value').text();
  $li.remove();

  $.ajax({
    type: 'DELETE',
    url: window.location.href.replace('edit', '/tags/') + id,
    data: {
      tag: {
        name: name
      }
    }
  });
};

window.destroyImage = function (image_id) {
  $.ajax({
    url: window.location.href.replace('edit', '/images/') + image_id,
    data: { image_id: image_id },
    method: 'delete'
  });
};

window.save_content = function (element) {
  $('#content_body').val($(element).closest('.card').find('.ql-editor').html());
  $(element).closest('form').submit();
};

window.addImageTagsToEditor = function () {
  const $editor = $('.ql-editor');

  $('.image_checkbox:checked').each(function (_, checkbox) {
    $editor.append(window.getImageTag($(checkbox).val()));
  });
};

window.addGalleryTagToEditor = function () {
  const $editor = $('.ql-editor');
  const ids = $('.image_checkbox:checked').map(function (_, checkbox) {
    return $(checkbox).val();
  }).toArray().join(',');

  $editor.append(window.getGalleryTag(ids));
};

window.removeImages = function (button) {
  const $button = $(button);

  $button.addClass('disabled');
  $('.image_checkbox:checked').map(function (_, checkbox) {
    window.destroyImage($(checkbox).val());
  });
  $button.removeClass('disabled');
};

window.getTag = function (tagName, options) {
  return ['[[' + tagName, window.mapOptionsToProps(options) + ']]'].join(' ');
};

window.mapOptionsToProps = function (options) {
  return $(Object.keys(options)).map(function (_, e) {
    return [e, '=', '"' + options[e] + '"'].join('');
  }).toArray().join(' ');
};

window.getImageTag = function (image_id) {
  return window.getTag('image', { id: String(image_id)});
};

window.getGalleryTag = function (image_ids) {
  return window.getTag('gallery', {
    id: Math.floor(Math.random() * 100),
    ids: String(image_ids),
    width: 200,
    height: 200
  });
};

window.uploadImage = function (url, button) {
  const $button = $(button);
  const $form = $button.closest('form');
  const formData = new FormData($form[0]);

  $form.find('#error, #success').html(null);
  $button.toggle('disable');

  $.ajax({
    url: url,
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    method: 'post'
  });
};

window.removeImage = function (image_id, model_name) {
  $('#image_' + image_id).remove();
  if ($('#list_of_images tbody tr').length < 1) {
    $('#list_of_images').html(`There are no images for this ${model_name}`);
    $('#image_action_buttons').hide();
  }
};
