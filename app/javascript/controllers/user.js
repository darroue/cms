const avatarTypes = [
  'male',
  'female',
  'human',
  'bottts',
  'avataaars',
  'gridy'
];

window.generateAvatarPreview = function (avatarTypes) {
  seed = window.getAvatarSeed();
  data = {
    row1: [],
    row2: []
  };

  $(avatarTypes).each(function (_, avatarType) {
    url = window.getAvatarUrl(avatarType, seed);
    td1 = '<td><a onclick="window.saveAvatarUrl(this);" data-url="#url"><img style="width: 50px; height: 50px;" class="rounded-circle m-1" src="#url"/></a></td>'.replace(/#url/g, url);
    td2 = '<td>' + avatarType + '</td>';

    data.row1.push(td1);
    data.row2.push(td2);
  });

  $tr1 = $('<tr/>').addClass('text-center').append(data.row1.join(''));
  $tr2 = $('<tr/>').addClass('text-center').append(data.row2.join(''));

  $('.avatar-preview tbody').html(null).append($tr1, $tr2);
};

window.getAvatarUrl = function (avatarType, seed) {
  return 'https://avatars.dicebear.com/api/' + avatarType + '/' + seed + '.svg';
};

window.getAvatarSeed = function () {
  return $('#user_nickname, #user_first_name').filter(function () { return this.value != ''; }).last().val();
};

window.saveAvatarUrl = function (element) {
  const $element = $(element);
  const value = $element.data('url');
  const activeClass = 'border border-primary';

  $('.avatar-preview img').removeClass(activeClass);
  $element.find('img').addClass(activeClass);

  $('.selected_avatar img').attr('src', value);
  $('#user_avatar_url').val(value);
};

window.reloadAvatars = function () {
  window.generateAvatarPreview(avatarTypes);
};
