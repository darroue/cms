module Activable
  extend ActiveSupport::Concern

  included do
    after_initialize :set_active, if: proc { self.respond_to? :active}
    scope :active, -> { where active: true }

    def set_active
      self.active = true if active.nil?
    end

    def active?
      self.active
    end
  end
end
