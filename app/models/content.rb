# == Schema Information
#
# Table name: contents
#
#  id           :bigint           not null, primary key
#  active       :boolean
#  body         :text
#  content_type :string
#  description  :text
#  published_at :datetime
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  author_id    :bigint
#  parent_id    :bigint
#  user_id      :bigint
#
# Indexes
#
#  index_contents_on_author_id  (author_id)
#  index_contents_on_parent_id  (parent_id)
#  index_contents_on_user_id    (user_id)
#
class Content < ApplicationRecord
  include ActiveStorageSupport::SupportForBase64
  self.inheritance_column = :content_type

  belongs_to :user, class_name: 'User', optional: true
  belongs_to :parent, class_name: 'Content', optional: true
  has_many :children, class_name: 'Content', foreign_key: :parent_id, dependent: :destroy
  has_many_base64_attached :images, dependent: :purge

  validates :title, :body, :published_at, presence: true

  before_create :generate_description

  def generate_description
    self.description = ActionView::Base.full_sanitizer.sanitize(body).truncate(150)
  end

  def all_attachments
    ActiveStorage::Attachment.where(record_id: [id, parent_id, child_ids].compact.uniq)
  end

  def all_images
    all_attachments.where(name: 'images')
  end

  def destroy
    all_images.each(&:purge) unless parent_id
    super
  end
end
