# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  active                 :boolean
#  avatar_url             :string
#  confirmation_sent_at   :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  failed_attempts        :integer          default(0), not null
#  first_name             :string
#  last_name              :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  locked_at              :datetime
#  nickname               :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  sex                    :string
#  sign_in_count          :integer          default(0), not null
#  unconfirmed_email      :string
#  unlock_token           :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise_opts = %i(database_authenticatable registerable rememberable timeoutable trackable)
  devise_opts += %i(lockable recoverable validatable confirmable) if ENV['SMTP_PASSWORD'].present?

  devise(*devise_opts)

  has_one :ban
  accepts_nested_attributes_for :ban

  has_and_belongs_to_many :roles

  after_create :assign_user_role

  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, uniqueness: true, unless: ->{ admin_update }
  validate :password_length, :password_lower_case, :password_uppercase, :password_contains_number, if: -> { password_confirmation.present? }

  attr_accessor :admin_update

  def password_length
    return if password.size >= 6
    errors.add :password, ' must at least 6 characters long '
  end

  def password_uppercase
    return if !!password.match(/\p{Upper}/)
    errors.add :password, ' must contain at least 1 uppercase '
  end

  def password_lower_case
    return if !!password.match(/\p{Lower}/)
    errors.add :password, ' must contain at least 1 lowercase '
  end

  def password_contains_number
    return if password.count("0-9") > 0
    errors.add :password, ' must contain at least one number'
  end

  def full_name
    [first_name, last_name].join(' ')
  end

  def display_name
    first_present?([
                     nickname,
                     full_name,
                     nickname_from_email
                   ])
  end

  def permissions
    @permissions ||= (roles.active.pluck(:permissions) + Role.find_by_name('Everyone').permissions).flatten.uniq.sort
  end

  def has_permission?(permission)
    permissions.include?(permission.to_s)
  end

  def active_for_authentication?
    super && active?
  end

  def inactive_message
    message = []
    unless active?
      admin_email = ENV['SMTP_USERNAME']
      message << 'User account was canceled!'
      if admin_email
        link_params = {
          subject: 'Account Restore',
          body: "Hi,\nI need to restore my account #{email} that I deactivated. Can you help?"
        }.to_param
        link = "<a href='mailto:#{admin_email}?#{link_params}'>#{admin_email}</a>"
        message << "If you wish to restore it, then you can contact administrator at: #{link}"
      end
    end

    message.join('<br/>')
  end

  def assign_user_role
    update roles: [Role.find_by_name('Users')] unless role_ids.present?
  end

  def destroy
    self.active = false
  end

  def active_ban?
    return false unless ban

    ban.active?
  end

  def nickname_from_email
    at_index = email.index('@')

    return email unless at_index

    email[0..email.index('@') - 1].titleize.humanize
  end
end
