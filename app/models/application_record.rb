class ApplicationRecord < ActiveRecord::Base
  include Activable

  self.abstract_class = true

  def first_present?(fields)
    fields.each do |field|
      return field if field.present?
    end

    false
  end
end
