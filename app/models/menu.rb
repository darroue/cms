# == Schema Information
#
# Table name: menus
#
#  id          :bigint           not null, primary key
#  description :text
#  name        :string
#  order       :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Menu < ActiveRecord::Base
  has_many :menu_items
  accepts_nested_attributes_for :menu_items

  validates :name, :description, presence: true, uniqueness: true

  scope :active, -> { all }
end
