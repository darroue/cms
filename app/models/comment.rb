# == Schema Information
#
# Table name: comments
#
#  id               :bigint           not null, primary key
#  active           :boolean
#  body             :text
#  commentable_type :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  author_id        :bigint
#  commentable_id   :integer
#  parent_id        :bigint
#  replying_to_id   :bigint
#  user_id          :bigint
#
# Indexes
#
#  index_comments_on_author_id       (author_id)
#  index_comments_on_parent_id       (parent_id)
#  index_comments_on_replying_to_id  (replying_to_id)
#  index_comments_on_user_id         (user_id)
#
class Comment < ApplicationRecord
  belongs_to :author, class_name: 'User', foreign_key: :author_id
  has_one :user
  belongs_to :parent, class_name: 'Comment', optional: true
  has_many :children, class_name: 'Comment', foreign_key: :parent_id, dependent: :destroy
  has_many :replies, class_name: 'Comment', foreign_key: :replying_to_id, dependent: :destroy
  has_many :ratings, as: :ratable
end
