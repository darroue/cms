# == Schema Information
#
# Table name: settings
#
#  id            :bigint           not null, primary key
#  default_value :string
#  name          :string
#  value         :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Setting < ApplicationRecord
  before_validation :set_default
  after_save :delete_cache

  validates :name, :value, :default_value, presence: true

  def set_default
    self[:value] = default_value unless value.present?
  end

  def delete_cache
    Rails.cache.delete(name)
  end
end
