# == Schema Information
#
# Table name: media
#
#  id          :bigint           not null, primary key
#  active      :boolean
#  description :text
#  link        :string
#  media_type  :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  parent_id   :bigint
#
# Indexes
#
#  index_media_on_parent_id  (parent_id)
#
class Medium < ApplicationRecord
  self.inheritance_column = :media_type

  has_one :parent, class_name: 'Medium', foreign_key: :parent_id
end
