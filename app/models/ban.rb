# == Schema Information
#
# Table name: bans
#
#  id          :bigint           not null, primary key
#  active      :boolean
#  reason      :string
#  valid_until :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :bigint           not null
#
# Indexes
#
#  index_bans_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Ban < ApplicationRecord
  belongs_to :user

  validates :valid_until, presence: true, if: -> { active? }
  scope :valid, ->() {
    where("active = true AND valid_until >= :date_time", date_time: DateTime.now)
  }

  def valid
    valid_until >= DateTime.now
  end

  def invalid
    !valid
  end

  def active?
    active && valid
  end
end
