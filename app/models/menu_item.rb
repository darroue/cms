# == Schema Information
#
# Table name: menu_items
#
#  id          :bigint           not null, primary key
#  description :text
#  link        :string
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  menu_id     :bigint           not null
#
# Indexes
#
#  index_menu_items_on_menu_id  (menu_id)
#
# Foreign Keys
#
#  fk_rails_...  (menu_id => menus.id)
#
class MenuItem < ActiveRecord::Base
  belongs_to :menu
end
