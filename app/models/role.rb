# == Schema Information
#
# Table name: roles
#
#  id          :bigint           not null, primary key
#  active      :boolean
#  name        :string
#  permissions :text
#  protected   :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Role < ApplicationRecord
  has_and_belongs_to_many :users
  serialize :permissions, Array

  validates :name, presence: true, uniqueness: true
  validate :check_protected!

  AVAILABLE_PERMISSIONS = {
    page: %i(index show add edit restore delete delete_version edit_own restore_own delete_own),
    post: %i(index show add edit restore delete delete_version edit_own restore_own delete_own),
    image: %i(add delete),
    comment: %i(show add edit restore delete edit_own restore_own delete_own),
    tag: %i(index show add delete),
    role: %i(index show add edit delete),
    user: %i(index show edit),
    widget: %i(edit restore delete_version),
    setting: %i(index edit),
    menu: %i(index show edit)
  }.freeze

  DEFAULT_PERMISSIONS = {
    administrators: AVAILABLE_PERMISSIONS,
    everyone: {
      page: %i(index show),
      post: %i(index show),
      comment: %i(show),
      tag: %i(index show)
    },
    users: {
      comment: %i(add edit_own delete_own)
    }
  }.freeze

  def self.permission_hash(permission_pairs = nil)
    permission_pairs ||= AVAILABLE_PERMISSIONS

    permission_pairs.each_with_object({}) do |(model, permissions), object|
      object[model] = permissions.map(& ->(perm) { [perm, "#{perm}_#{model}"] })
    end
  end

  def self.permission_list(permission_pairs)
    permission_hash(permission_pairs).each_with_object([]) { |(_k, v), o| o.concat(v.map(&:second)) }
  end

  def self.available_permissions
    permission_list(AVAILABLE_PERMISSIONS)
  end

  def self.default_permissions(role)
    permission_list(DEFAULT_PERMISSIONS[role])
  end

  def permission_hash
    @permission_hash ||= permissions
  end

  def check_protected!
    errors.add :base, "You can't edit protected Role!" if persisted? && protected?
  end

  def protected?
    protected
  end

  def everyone?
    name == 'Everyone'
  end
end
