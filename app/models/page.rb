# == Schema Information
#
# Table name: contents
#
#  id           :bigint           not null, primary key
#  active       :boolean
#  body         :text
#  content_type :string
#  description  :text
#  published_at :datetime
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  author_id    :bigint
#  parent_id    :bigint
#  user_id      :bigint
#
# Indexes
#
#  index_contents_on_author_id  (author_id)
#  index_contents_on_parent_id  (parent_id)
#  index_contents_on_user_id    (user_id)
#
class Page < Content
  belongs_to :author, class_name: 'User'
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :ratings, as: :ratable, dependent: :destroy
  has_and_belongs_to_many :tags, foreign_key: :content_id
end
