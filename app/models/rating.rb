# == Schema Information
#
# Table name: ratings
#
#  id           :bigint           not null, primary key
#  ratable_type :string
#  score        :decimal(, )
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  ratable_id   :integer
#
class Rating < ActiveRecord::Base
  self.inheritance_column = :ratable_type
end
