# == Schema Information
#
# Table name: media
#
#  id          :bigint           not null, primary key
#  active      :boolean
#  description :text
#  link        :string
#  media_type  :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  parent_id   :bigint
#
# Indexes
#
#  index_media_on_parent_id  (parent_id)
#
class Image < Medium
end
