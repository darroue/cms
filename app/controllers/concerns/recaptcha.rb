module Recaptcha
  extend ActiveSupport::Concern

  def recaptcha_available?
    ENV['RECAPTCHA_SITE_KEY'].present? && ENV['RECAPTCHA_SECRET_KEY'].present?
  end
end
