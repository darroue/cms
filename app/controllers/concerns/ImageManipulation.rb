module ImageManipulation
  extend ActiveSupport::Concern
  include ImagesHelper

  included do
    before_action :remove_data_images_from_body_param, only: %i(create update)
    before_action :replace_body_image_tag_with_image, only: %i(show)
    after_action :replace_image_placeholders_with_image_tags, only: %i(create update)
  end

  def replace_content_attribute_image_tag_with_image(attribute)
    string = @content.send(attribute)

    string = replace_image_tag_with_image(string)
    string = replace_gallery_tag_with_gallery(string)

    @content.send(:"#{attribute}=", string)
  end

  def replace_body_image_tag_with_image
    replace_content_attribute_image_tag_with_image(:body)
  end

  def replace_image_placeholders_with_image_tags
    return unless @content.valid?

    content_body = @content.body

    content_body.scan(/\[\[image_placeholder [^\]]+\]\]/).each do |image_tag|
      tag_attributes = get_tag_attributes(image_tag)
      image_data = @images[tag_attributes[:id]]

      if image_data
        @content.images.attach(data: image_data)
        tag_attributes[:id] =  @content.images.last.id
        content_body = content_body.sub(image_tag, to_my_tag(:image, tag_attributes))
      end
    end

    @content.body = content_body
    @content.save
  end

  def remove_data_images_from_body_param
    @images = {}
    model_params_body = model_params[:body]

    image_tags = model_params_body.scan(/(<img [^>]+>)/).flatten
    image_tags.each_with_index do |image_tag, index|
      index = index.to_s
      tag_attributes = get_tag_attributes(image_tag)
      src = tag_attributes.delete(:src)

      if src.try(:starts_with?, 'data:')
        @images[index] = src
        tag_attributes[:id] = index
        model_params_body = model_params_body.sub(image_tag, to_my_tag(:image_placeholder, tag_attributes))
      end
    end

    model_params[:body] = model_params_body if image_tags.present?
  end
end
