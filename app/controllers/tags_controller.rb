class TagsController < CRUDController
  before_action :check_model_params, only: %i(create_tags delete_tags)
  before_action :find_parent_content, except: %i(show index)

  def index
    @contents = model.includes(:contents, :contents_tags).order("unaccent(lower(tags.name)) ASC")
  end

  def check_permissions
    super case action_name
      when 'create_tags'
        'add'
      when 'delete_tags'
        'delete'
      end
  end

  def create_tags
    date_time = DateTime.now
    data = model_params[:names].split(',').map(&:strip).select(&:present?).uniq.map(& lambda { |name|
      { name: name.capitalize, updated_at: date_time, created_at: date_time }
    })

    # Bulk update
    result = @model.upsert_all(data, unique_by: :name)
    previous_tags = @parent_content.tag_ids
    tags = @model.where(id: result.rows.flatten)
    @parent_content.tags = @parent_content.tags | tags
    @tags = @model.where(id: @parent_content.tag_ids - previous_tags)
    @model_name_s = @parent_content.class.name.downcase

    render 'new_tags.js'
  end

  def delete_tags
    @parent_content.tags = []

    render js: "$('#names').parent().find('ul').html(null);"
  end

  def destroy
    @parent_content.tags = @parent_content.tags - [@content]

    render js: "$('#collapse_tags').find(\".value:contains('#{@content.name}')\").closest('.list-group-item').remove();"
  end

  private

  def find_parent_content
    @parent_content = Content.find_by(id: params[:page_id] || params[:post_id])
    head :not_found unless @parent_content
  end

  def model_params
    params.permit(:names, :page_id, :post_id)
  end

  def check_model_params
    unless (action_name == 'delete_tags' ? true : model_params[:names].present?) && (params[:page_id] || params[:post_id])
      head :bad_request
    end
  end

  def model
    @model ||= Tag
  end
end
