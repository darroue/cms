class SearchController < ApplicationController
  skip_before_action :check_permissions, only: %i(show)

  def show
    @search_string = params[:value]
    @contents = Content.where(parent_id: nil)
      .where(
        sql_query(%i(title description body)),
        search_string: "%#{I18n.transliterate(@search_string || '').downcase}%"
      )
      .page(params[:page]).per(25)
  end

  def sql_query(fields)
    fields.map do |field|
      "lower(unaccent(#{field})) LIKE :search_string"
    end.join(' OR ')
  end
end
