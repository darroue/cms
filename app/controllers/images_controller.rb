class ImagesController < ApplicationController
  skip_before_action :check_permissions, only: %i(show)
  before_action :set_content, only: %i(create destroy)
  protect_from_forgery except: :show

  def show
    image_id = image_params[:id]
    head :forbidden unless image_id

    image = ActiveStorage::Attachment.find_by_id(image_id)

    url = if image
      param_width = image_params[:width]
      param_height = image_params[:height]
      param_rotate = image_params[:rotate]
      param_preview = image_params[:preview]
      blob_width = image.try(:blob).try(:metadata).try(:[], :width)
      blob_height = image.try(:blob).try(:metadata).try(:[], :height)
  
      variant_options = if param_preview.present?
        default_size = 200
        size_limit = [param_width, param_width].max || default_size

        {
          resize: "#{param_width || default_size}x#{param_width || default_size}",
          gravity: :center,
          background: :white,
          extent: "#{size_limit}x#{size_limit}"
        }
      elsif param_width.present? || param_height.present?
        {
          resize_to_fit: [param_width, param_height]
        }
      else
        {}
      end

      variant_options[:rotate] = param_rotate if param_rotate.present?

      if variant_options.present? &&
        (
          (
            param_width.present? && blob_width.present? &&
            param_width.to_i <= blob_width.to_i
          ) ||
          (
            param_height.present? && blob_height.present? &&
            param_height.to_i <= blob_height.to_i
          )
        )

        image.variant(**variant_options)        
      else
        rails_blob_path(image)
      end
    end

    redirect_to url
  end

  def create
    extend ActionView::Helpers::NumberHelper

    images = params[:images]
    @image_ids = []

    @errors = if images.present?
      images.each_with_object([]) do |image, array|
        result = validate_image(image)

        if result.present?
          array << result
        else
          if @content.images.attach(image)
            @image_ids << @content.images.last.id
          else
            array << error_message(image)
          end
        end
      end
    else
      array << "No image(s) selected!"
    end

    @message = if @errors.any?
      @errors = @errors.join('<br/>').html_safe

      nil
    else
      'All images were successfully uploaded'
    end

    render(&:js)
  end

  def error_message(image, type = nil)
    "#{image.original_filename} - " + case type
    when :size_limit
      "File is too big! (#{number_to_human_size(image.size)}) Limit is 5MB"
    when :not_image
      "Selected file is not a image!"
    else
      "Error when uploading image!"
    end
  end

  def validate_image(image)
    if image.content_type.match(/image\/\w+/)
      if image.size > 5.megabyte
        error_message(image, :size_limit)
      end
    else
      error_message(image, :not_image)
    end
  end

  def destroy
    image_id = params[:image_id]

    if image_id
      if image = @content.images.find_by_id(image_id)
        if image.purge
          render js: "window.removeImage(#{image_id}, '#{@content.class.name}');"
        else
          head 500
        end
      else
        head :not_found
      end
    else
      head :forbidden
    end
  end

  private

  def set_content
    @content = Content.find_by_id(params[:page_id] || params[:post_id] || params[:widget_id])

    head :not_found unless @content
  end

  def image_params
    params.permit(*%i(
      id
      width
      height
      rotate
      format
      preview
    ))
  end
end
