class ApplicationController < ActionController::Base
  SKIP_AUTHENTICATION = {
    sessions: %i(new),
    users: %i(),
    registrations: %i(new),
    passwords: %i(new),
    confirmations: %i(new),
    unlocks: %i(new)
  }.freeze

  before_action :authenticate_user!, except: %i(show index), unless: lambda {
    SKIP_AUTHENTICATION[controller_name.to_sym].try(:include?, action_name.to_sym)
  }
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :check_ban, unless: -> { ['/', '/posts'].include?(request.path) }
  before_action :check_permissions, unless: -> { ['/', '/posts'].include?(request.path) }
  before_action :prepend_view_paths
  
  protected

  def prepend_view_paths
    themes_path = File.join(Rails.root, 'app/views/themes')

    [
      File.join(themes_path, 'default'),
      File.join(themes_path, theme)
    ].each do |path|
      prepend_view_path(path)
    end
  end

  def theme
    Settings.get(__method__)
  end
  
  def check_ban
    if current_user.try(:active_ban?)
      ban = @current_user.ban
      valid_until = ban.valid_until
      reason = ban.reason.empty? ? 'Not specified' : ban.reason

      flash_message = [
        "<strong>Your account have been BANNED until #{I18n.l(valid_until)}!</strong>",
        "Reason: #{reason}",
        '',
        '<strong>You have been signed out!</strong>'
      ].join('<br/>')
      flash[:error] = flash_message

      sign_out current_user
      redirect_to root_path
    end
  end

  def check_permissions(permission_action = nil)
    return if devise_controller?

    permission_action ||= case action_name
                          when 'new', 'create'
                            'add'
                          when 'edit', 'update'
                            'edit'
                          when 'destroy'
                            'delete'
                          else
                            action_name
                          end

    model_name = try(:model_name_s) || controller_name.singularize
    permission = "#{permission_action}_#{model_name}"
    version_permission = "#{permission_action}_version_#{model_name}"
    own_permission = "#{permission_action}_own_#{model_name}"

    unless can?(permission) || can?(version_permission) || can?(own_permission)
      show_permission_error
    end
  end

  def show_permission_error
    flash[:error] = "You don't have required permission to perform this action!"

    redirect_back(fallback_location: root_path)
  end

  def configure_permitted_parameters
    user_keys = %i(first_name last_name nickname sex email avatar_url password password_confirmation)

    devise_parameter_sanitizer.permit(:sign_up, keys: user_keys)
    devise_parameter_sanitizer.permit(:account_update, keys: user_keys)
  end

  def can?(permission, model = nil)
    if model
      can?([permission, model].join('_').to_sym)
    else
      if current_user
        current_user.has_permission?(permission)
      else
        @everyone_permissons ||= Role.find_by_name('Everyone').permissions

        @everyone_permissons.include?(permission.to_s)
      end
    end
  end
end
