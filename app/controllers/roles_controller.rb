class RolesController < CRUDController
  layout 'wide'

  private

  # Only allow a list of trusted parameters through.
  def model_params
    params.require(:role).permit(
      :name, :active, permissions: [], user_ids: []
    )
  end

  def model
    @model ||= Role
  end
end
