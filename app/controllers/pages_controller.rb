class PagesController < ContentsController
  include ImageManipulation

  private

  # Only allow a list of trusted parameters through.
  def model_params
    @model_params ||= params.require(model.name.downcase.to_sym).permit(
      :title, :published_at, :description, :body
    )
  end

  def model
    @model ||= Page
  end
end
