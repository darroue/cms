class SettingsController < ApplicationController
  layout 'wide'

  before_action :set_settings, only: %i(generic update)

  def generic; end

  def index
    @admin_pages = %w(generic_settings roles users menus)
  end

  def update
    params[:settings].each_pair do |id, value|
      Setting.find(id).update!(value: value)
    end

    flash[:success] = 'Settings were successfully updated'
    redirect_to admin_path
  end

  def check_permissions
    super case action_name
          when 'generic'
            'edit'
          end
  end

  private

  def set_settings
    @settings = Setting.all
  end
end
