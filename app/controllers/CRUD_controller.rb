class CRUDController < ApplicationController
  layout -> { select_layout }
  before_action :set_content, only: %i(show edit update destroy)

  # Helpers
  before_action :helper_instance_variables

  def index
    @contents = model.all
  end

  def show; end

  def new
    @content = model.new
  end

  def edit; end

  def create
    @content = model.new(model_params)

    yield if block_given?

    respond_to do |format|
      if @content.save
        flash[:success] = "#{model_name} was successfully created."

        format.html { redirect_to @content }
        format.json { render :show, status: :created, location: @content }
      else
        format.html { render :new }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    yield if block_given?

    respond_to do |format|
      if @content.update(model_params)
        flash[:success] = "#{model_name} was successfully updated."

        format.html { redirect_to @content }
        format.json { render :show, status: :ok, location: @content }
      else
        format.html { render :edit }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    content_parent_id = @content.parent_id

    if @content.destroy
      respond_to do |format|
        if content_parent_id
          flash[:success] = "#{model_name} version was successfully destroyed."

          format.html { redirect_to controller: model_name_p.downcase, action: :edit, id: content_parent_id }
        else
          flash[:success] =  "#{model_name} was successfully destroyed."

          format.html { redirect_to controller: model_name_p.downcase, action: :index}
        end
  
        format.json { head :no_content }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_content
    @content = model.find(params[:id])
  end

  def model_name
    @model_name ||= model.name
  end

  def model_name_s
    @model_name_s ||= model_name.downcase.to_sym
  end

  def model_name_p
    @model_name_p ||= model_name.pluralize
  end

  def helper_instance_variables
    model
    model_name
    model_name_s
    model_name_p
  end

  def select_layout
    case action_name.to_sym
    when :new, :edit
      'wide'
    else
      'application'
    end
  end
end
