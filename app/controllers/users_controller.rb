class UsersController < CRUDController
  layout 'wide'

  before_action :build_ban, only: :edit

  def update
    super do
      @content.admin_update = true
    end
  end

  private

  # Only allow a list of trusted parameters through.
  def model_params
    trusted_params = params.require(:user).permit(
      :active, :first_name, :last_name, :nickname, :sex, :avatar_url, {
        ban_attributes: %i(active valid_until reason id)
      }
    )
  end

  def model
    @model ||= User
  end

  def build_ban
    @content.build_ban.update(active: false) unless @content.ban
  end
end
