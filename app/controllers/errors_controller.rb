class ErrorsController < ApplicationController
  layout 'wide'
  skip_before_action :check_permissions, only: %i(show)

  def show
    @icon_name, @error_message = case params[:code]
    when '404'
      ['file-code', 'Page not found']
    when '422'
      ['exclamation', 'Unprocessable Entity']
    when '500'
      ['bug', 'Internal server error']
    end

    @error_message = 'Undefined Error' unless @error_message
  end
end
