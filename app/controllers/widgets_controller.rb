class WidgetsController < ContentsController
  def update
    @content.user = current_user
    @content.author = current_user
    create_new_version

    respond_to do |format|
      if @content.update(model_params)
        flash[:success] = "#{model_name} was successfully updated."

        format.html { redirect_to root_path }
      else
        format.html { render :edit }
      end
    end
  end

  def restore
    set_content
    respond_to do |format|
      if restore_version
        flash[:success] = "#{model_name} version was successfully restored."

        format.html { redirect_to root_path }
      end
    end
  end

  private

  # Only allow a list of trusted parameters through.
  def model_params
    params.require(:widget).permit(
      :title, :body
    )
  end

  def model
    @model ||= Widget
  end
end
