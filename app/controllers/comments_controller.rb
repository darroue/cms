class CommentsController < ContentsController
  before_action :set_parent_content
  before_action :set_content, except: %i(create new restore)
  skip_before_action :check_permissions, only: %i(get_versions)

  def create
    @content = @parent_content.comments.new(model_params.merge(author: current_user))

    respond_to do |format|
      if @content.save
        flash[:success] = 'Comment was successfully created.'

        format.html { redirect_to @parent_content }
      end
    end
  end

  def update
    respond_to do |format|
      create_new_version
      if @content.update(model_params.merge(user_id: current_user.id))
        flash[:success] = 'Comment was successfully updated.'

        format.html { redirect_to @parent_content }
      end
    end
  end

  def destroy
    @content.destroy
    respond_to do |format|
      flash[:success] = 'Comment was successfully destroyed.'

      format.html { redirect_to @parent_content}
    end
  end

  def get_versions
    output = @content.children.map do |content|
      { id: content.id, updated_at: I18n.l(content.updated_at), body: content.body }
    end

    render js: output.to_json
  end

  def restore
    @content = @parent_content.comments.find(params[:version_id])
    @content_parent = @content.parent

    respond_to do |format|
      if restore_version
        flash[:success] = "#{model_name} version was successfully restored."

        format.html { redirect_to @parent_content }
      end
    end
  end

  private

  def set_parent_content
    @parent_content = Content.find(params[:page_id] || params[:post_id])
  end

  def set_content
    @content = @parent_content.comments.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def model_params
    params.require(:comment).permit(
      :body, :replying_to_id
    )
  end

  def model
    @model ||= Comment
  end
end
