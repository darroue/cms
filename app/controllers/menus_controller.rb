class MenusController < CRUDController
  layout 'wide'

  def update
    super do
      @content.menu_items.each(&:delete)
      params[:menu_items].values.each do |menu_item|
        @content.menu_items.create!(menu_item)
      end
    end
  end

  private

  # Only allow a list of trusted parameters through.
  def model_params
    params.require(:menu).permit(
      :name, :description
    )
  end

  def model
    @model ||= Menu
  end
end
