class ContentsController < CRUDController
  def index
    @contents = model.includes(:tags, :author)
                     .active
                     .where("#{model.table_name}.published_at < ?", Time.now)
                     .order(published_at: :desc)
                     .page(params[:page]).per(25)
  end

  def unpublished
    @contents = model.includes(:tags, :author)
                     .active
                     .where("#{model.table_name}.published_at > ?", Time.now)
                     .order(published_at: :desc)
                     .page(params[:page]).per(25)
  end

  def check_permissions
    super case action_name
          when 'unpublished'
            'index'
          end
  end

  def show
    @content_comments = @content.comments.includes(
      :author,
      :replies
    ).active.where(replying_to_id: nil).where.not(user_id: Ban.valid.pluck(:user_id))
  end

  def create
    super do
      @content.author = current_user
      @content.user = current_user
    end
  end

  def update
    super do
      create_new_version
      @content.user = current_user
    end
  end

  def restore
    set_content
    respond_to do |format|
      if restore_version
        flash[:success] = "#{model_name} version was successfully restored."

        format.html { redirect_to @content.parent }
      end
    end
  end

  def remove_same_versions
    @content.parent.children.where(body: @content.body).map(&:delete)
  end

  private

  def create_new_version(source = nil)
    source ||= @content
    source_content = source.dup

    columns_to_update = source.attributes.slice('updated_at', 'created_at').merge(active: false, parent_id: source.id)
    source_content.update(columns_to_update)
  end

  def restore_version
    @content_parent ||= @content.parent

    create_new_version(@content_parent)
    remove_same_versions

    @content_parent.update(@content.attributes.slice(
      'title',
      'body',
      'updated_at',
      'created_at'
    ).merge(user_id: @current_user.id))
  end
end
