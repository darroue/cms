module ImagesHelper
  include ActionView::Helpers::TagHelper

  def get_tag_attributes(tag)
    tag_attributes = {}
    tag.scan(/(\w+)=\"([^"]+)\"/).each do |scan|
      tag_attributes[scan.first.to_sym] = scan.last
    end
  
    tag_attributes
  end

  def map_tag_attributes(hash)
    hash.map {|key, value| "#{key}=\"#{value}\""}.join(' ')
  end

  def to_my_tag(type, hash)
    "[[#{type} #{map_tag_attributes(hash)}]]"
  end

  def to_html_tag(type, hash, &block)
    if block_given?
      %Q(
        <#{type} #{map_tag_attributes(hash)}>
          #{yield}
        </#{type}>
      )
    else
      "<#{type} #{map_tag_attributes(hash)} />"
    end
  end

  def replace_image_tag_with_image(string)
    string.scan(/\[\[image [^\]]+\]\]/).each do |image_tag|
      image_attributes = get_tag_attributes(image_tag)
      image_attributes[:class] = [
        image_attributes[:class],
        "load-with-js"
      ].compact.join(' ')

      string.sub!(image_tag, to_html_tag(:img, image_attributes))
    end

    string
  end

  def image_url(image_id, options = {})
    ("/images/#{image_id}?" + options.to_param).html_safe
  end

  def html_image_tag(gallery_id, image_id, options = {})
    to_html_tag(:a, {
      href: image_url(image_id)
    }) do
      to_html_tag(:img, {
        id: image_id,
        class: 'img-thumbnail mb-1 load-with-js',
        **options
      })
    end
  end

  def replace_gallery_tag_with_gallery(string)
    string.scan(/\[\[gallery [^\]]+\]\]/).each do |image_tag|
      attributes = get_tag_attributes(image_tag)
      options = { preview: true }
      width = attributes.dig(:width)
      height = attributes.dig(:height)
      gallery_id = attributes.dig(:id)
      image_ids = attributes.dig(:ids)

      options[:width] = width if width.present?
      options[:height] = height if height.present?
      
      new_string = if gallery_id.present? && image_ids.present?
        to_html_tag(:div, {
          class: 'lightgallery',
        }) do
          image_ids.split(',').map do |image_id|
            html_image_tag(gallery_id, image_id, options)
          end.join("\n")
        end
      else
        ''
      end

      string.sub!(image_tag, new_string)
    end

    string
  end
end
