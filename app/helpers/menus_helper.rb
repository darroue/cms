module MenusHelper
  def render_js_menu_items
    @content.menu_items.map do |menu_item|
      js_method_params = menu_item.slice(:id, :title, :description, :link).values.inspect

      "menuItemTemplate(...#{js_method_params})"
    end.join(', ').html_safe
  end
end
