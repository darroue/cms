module ContentsHelper
  def form_text_field(form, field_name, label_width: 3, readonly: false)
    input_options = readonly ? { class: 'form-control-plaintext', readonly: true } : { class: 'form-control' }

    content_tag(:div, class: 'form-group row') do
      concat(
        content_tag(:div, class: "col-sm-#{label_width}") do
          form.label field_name, class: 'col-form-label'
        end
      )
      concat(
        content_tag(:div, class: 'col-sm') do
          form.text_field(field_name, **input_options)
        end
      )
    end
  end

  def form_checkbox(form, field_name, readonly: false)
    input_options = readonly ? { class: 'form-check-input', disabled: true } : { class: 'form-check-input' }

    content_tag(:div, class: 'form-group form-check') do
      concat(form.check_box(field_name, **input_options))
      concat(form.label(field_name, class: 'form-check-label'))
    end
  end

  def form_datetime_field(form, field_name, label_width: 3, readonly: false)
    input_options = readonly ? { class: 'form-control', readonly: true } : { class: 'form-control' }

    content_tag(:div, class: 'form-group row') do
      concat(
        content_tag(:div, class: "col-sm-#{label_width}") do
          form.label field_name, class: 'col-form-label'
        end
      )
      concat(
        content_tag(:div, class: 'col-sm') do
          form.datetime_field(field_name, **input_options)
        end
      )
    end
  end

  def form_radio_button_data_collection(form, field_name, data, label_width: 3, readonly: false)
    input_options = readonly ? { class: 'custom-control-input', disabled: true } : { class: 'custom-control-input' }

    content_tag(:div, class: 'form-group row') do
      concat(
        content_tag(:div, class: "col-sm-#{label_width}") do
          form.label field_name, class: 'col-form-label'
        end
      )
      concat(
        content_tag(:div, class: 'custom-control custom-radio custom-control-inline') do
          data.each do |entry|
            concat(
              content_tag(:div, class: 'col-sm') do
                concat(form.radio_button(field_name, entry, **input_options))
                concat(form.label(:sex, value: entry, class: 'custom-control-label'))
              end
            )
          end
        end
      )
    end
  end

  def clickable_clazz(clickable)
    "text-#{clickable ? 'primary clickable' : 'secondary'}"
  end

  def collapse_id(name)
    "collapse_#{name.to_s.parameterize}"
  end

  def collapse_link(id, expanded, &block)
    content_tag(:a,
      data: {
        toggle: :collapse
      },
      href: "##{id}",
      aria: {
        expanded: expanded,
        controls: id
      }) do
        yield
    end
  end

  def collapse_body(id, expanded, &block)
    content_tag(:div, id: id, class: 'collapse' + (expanded ? ' show' : '')) do
      yield
    end
  end

  def collapsable(f, name, expanded = true, &block)
    id = collapse_id(name)

    content_tag(:div, class: 'form-group') do
      concat(
        collapse_link(id, expanded) do
          content_tag(:div, class: 'label') do
            f.label name, class: "clickable"
          end
        end
      )

      concat(
        collapse_body(id, expanded) do
          yield
        end
      )
    end
  end

  def collapsable_card(name, expanded = true, &block)
    id = collapse_id(name)

    content_tag(:div, class: 'card') do
      concat(
        content_tag(:div, class: 'card-header') do
          collapse_link(id, expanded) do
            name
          end
        end
      )

      concat(
        collapse_body(id, expanded) do
          content_tag(:div, class: 'card-body') do
            yield
          end
        end
      )
    end
  end

  def add_image_tag_to_editor(image_id, clickable = false)
    content_tag(:span, 'Add to editor', class: clickable_clazz(clickable), onclick: "window.addImageTagToEditor(#{image_id});")
  end

  def upload_image_path
    url_for(controller: :images, action: :create, method: :post, :"#{@model_name_s}_id" => @content.id)
  end

  def destroy_image(image_id, clickable = false)
    url = url_for(controller: :images, action: :destroy, method: :delete, :"#{@content.class.name.downcase}_id" => @content.id, id: image_id)
    content_tag(:span, 'Delete image', class: clickable_clazz(clickable), onclick: "window.destroyImage('#{url}', #{image_id});")
  end

  def image_actions(image_id, clickable = false)
    content_tag(:div) do
      concat(add_image_tag_to_editor(image_id, clickable))
      concat(content_tag(:br))
      concat(destroy_image(image_id, clickable)) if can?(:delete_image)
    end
  end

  def image_options(image_id)
    content_tag(:div) do
      %w(width height rotate).each do |field_name|
        field_id = :"image_#{image_id}_#{field_name}"
    
        concat(
          content_tag(:div, class: 'form-group row') do
            concat(
              content_tag(:div, class: "col") do
                label_tag field_id, field_name.capitalize, class: 'col-form-label'
              end
            )
            concat(
              content_tag(:div, class: 'col') do
                number_field_tag(field_id, nil, class: 'form-control')
              end
            )
          end
        )
      end
    end
  end
end
