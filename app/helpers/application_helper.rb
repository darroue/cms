require_relative 'application/settings'

module ApplicationHelper
  def flash_message(flash_type, messages)
    return unless messages.present?

    messages = [messages] if messages.is_a?(String)

    content_tag :div, class: "alert alert-#{flash_type}" do
      messages.each do |message|
        concat(content_tag(:p, message.html_safe, class: 'my-0'))
      end
    end
  end

  def can?(permission, model = nil)
    if model
      can?([permission, model].join('_').to_sym)
    else
      if current_user
        current_user.has_permission?(permission)
      else
        @everyone_permissons ||= Role.find_by_name('Everyone').permissions

        @everyone_permissons.include?(permission.to_s)
      end
    end
  end

  def action_button(label, action, type = nil, id = nil, options: {})
    link_args = {}
    link_args[:id] = id if id
    options[:data] = (options[:data] || {})
    options[:data][:disable_with] = 'Processing...'
    options[:class] = %w(btn)

    button_type = if action.in?(%i(new edit submit))
      :primary
    elsif action.in?(%i(show index))
      :secondary
    elsif action.in?(%i(destroy delete))
      options[:data].merge!(confirm: 'Are you sure?', method: 'delete')

      :danger
    elsif action.in?(%i(primary secondary warning danger))
      options[:data].delete(:disable_with)

      action
    else
      :warning
    end

    if type == :admin
      options[:class] << "btn-outline-#{button_type}"
    else
      options[:class] << "btn-#{button_type}"
    end

    if type == :user
      options[:class] << 'btn-sm'
    end

    link_args = if action.in?(%i(primary secondary warning danger submit destroy))
      nil
    elsif action.in?(%i(delete))
      { action: :destroy }
    else
      { action: action }
    end
    
    options[:onclick] = "$(this).closest('form').submit()" if action.in?(%i(submit))
    # options[:type] = 'button' # We dont really need this and WebView on iOS makes buttons ugly because of this
    link_options = link_args ? { href: url_for(**link_args) } : { tabindex: 0 }

    content_tag :a, label, **link_options, **options
  end

  def action_link(label, clazz = :primary, onclick: {})
    content_tag(:a, class: "text-#{clazz} clickable", onclick: onclick) do
      content_tag(:li, label, class: "list-group-item")
    end
  end

  def current_layout
    controller.class.send(:_layout)
  end

  def tag_size(size)
    case size
    when 0..10
      0
    when 10..25
      1
    when 25..50
      2
    when 50..75
      3
    when 75..100
      4
    else
      5
    end
  end

  def can_modify_content?
    can?(:add, @model_name_s) || can?(:edit, @model_name_s) || can?(:restore, @model_name_s) || can?(:delete, @model_name_s)
  end
end
