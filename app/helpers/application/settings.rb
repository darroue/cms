class Settings
  def self.get(name)
    Rails.cache.fetch(name) { Setting.find_by_name(name)&.value }
  end
end
